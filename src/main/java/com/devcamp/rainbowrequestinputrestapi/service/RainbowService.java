package com.devcamp.rainbowrequestinputrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    public ArrayList<String> allRainbows() {
        ArrayList<String> rainbows = new ArrayList<>();
        rainbows.add("red");
        rainbows.add("orange");
        rainbows.add("yellow");
        rainbows.add("green");
        rainbows.add("blue");
        rainbows.add("indigo");
        rainbows.add("violet");
        return rainbows;

    }

    public ArrayList<String> findRainbowByChar(String paramFilter) {
        ArrayList<String> rainbows = allRainbows();
        ArrayList<String> rainbowResult = new ArrayList<>();
        for (String string : rainbows) {
            if (string.contains(paramFilter)) {
                rainbowResult.add(string);
            }
        }
        return rainbowResult;

    }

    public String rainbow(int paramIndex) {
        ArrayList<String> rainbows = allRainbows();
        String result = "";

        if (paramIndex < -1 || paramIndex > 6) {
            return "";
        } else {
            result = rainbows.get(paramIndex);
        }
        return result;
    }

}
