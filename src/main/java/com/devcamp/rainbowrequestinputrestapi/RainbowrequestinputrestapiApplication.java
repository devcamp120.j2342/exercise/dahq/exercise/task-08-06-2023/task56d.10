package com.devcamp.rainbowrequestinputrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowrequestinputrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowrequestinputrestapiApplication.class, args);
	}

}
