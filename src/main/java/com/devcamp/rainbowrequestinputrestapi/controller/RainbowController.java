package com.devcamp.rainbowrequestinputrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rainbowrequestinputrestapi.service.RainbowService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RainbowController {
    @Autowired
    private RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> rainbowFilter(@RequestParam(required = true, name = "string") String string) {
        ArrayList<String> arrayList = rainbowService.findRainbowByChar(string);
        return arrayList;
    }

    @GetMapping("/rainbow-request-param/{index}")
    public String rainbowIndex(@PathVariable int index) {
        String rainbow = rainbowService.rainbow(index);
        return rainbow;
    }

}
